module.exports = (ngModule) => {
    // Directive declaration.
    ngModule.directive('imageUploader', ($compile) => {
        return {
            compile: () => {
                let pGetTemplatePromise = new Promise((resolve) => {
                    require.ensure([], () => resolve(require('./image-uploader.html')));
                });

                return (scope, element) => {
                    pGetTemplatePromise
                        .then((template) => {
                            element.html($compile(template)(scope));
                        });
                };
            },
            restrict: 'E',
            transclude: {},
            scope: {
                ngOnClose: '&',
                ngOnImageCrop: '&'
            },
            controller: function (FileUploader,
                                  $ui,
                                  $element, $window,
                                  $scope) {

                //#region Methods

                // Image selector which is for selecting avatar to be cropped.
                $scope._avatarSelector = new FileUploader();

                // The selected image.
                $scope._selectedImage = null;

                // The cropped image.
                $scope._croppedImage = null;

                $scope.oModel = {
                    croppedImage: null,
                    base64CroppedImage: null
                };

                //#endregion

                //#region Methods

                /*
                * Called when component is initialized.
                * */
                $scope.ngOnInit = () => {

                };

                /*
                * Called when confirm button is clicked.
                * */
                $scope.ngOnOkClick = () => {
                    $scope.ngOnImageCrop({
                        image: $scope.oModel.croppedImage,
                        base64Image: $scope.oModel.base64CroppedImage
                    });
                };

                /*
                * Called when cancel button is clicked.
                * */
                $scope.ngOnClose = () => {
                    $scope.ngOnClose();
                };

                //#endregion

                //#region Events

                /*
                * Called when avatar is selected.
                * */
                $scope._avatarSelector.onAfterAddingFile = (fileItem) => {
                    let file = fileItem._file;
                    $scope._selectedImage = file;
                }

                //#endregion
            }
        }
    });
};