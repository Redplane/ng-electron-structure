module.exports = (ngModule) => {
    // Directive declaration.
    ngModule.directive('sideBar', ($ui, $state, $compile) => {
        return {
            compile: () => {
                let pGetTemplatePromise = new Promise((resolve) => {
                    require.ensure([], () => resolve(require('./side-bar.html')));
                });

                return (scope, element) => {
                    pGetTemplatePromise
                        .then((template) => {
                            element.html($compile(template)(scope));
                        });
                };
            },
            restrict: 'E',
            transclude: {},
            scope: {},
            controller: function (
                urlStatesConstant,
                $scope, $timeout) {

                //#region Properties

                // Constants relection.
                $scope.urlStatesConstant = urlStatesConstant;

                //#endregion

                //#region Methods

                // Called when directive is initialized.
                $scope.ngOnInit = () => {

                };

                /*
                * Check whether current state contain a specific state or not.
                * */
                $scope.bContainState = (stateName) => {
                    return $state.includes(stateName);
                };

                /*
                * Called automatically when component is initialized.
                * */
                $timeout(() => {
                    $ui.initSlimScrollbar('nav.side-navbar', {
                        scrollInertia: 200
                    });
                });
                //#endregion
            }
        }
    });
};