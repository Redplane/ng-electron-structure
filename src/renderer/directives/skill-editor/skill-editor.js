module.exports = (ngModule) => {
    // Directive declaration.
    ngModule.directive('skillEditor', ($compile) => {
        return {
            compile: () => {
                let pGetTemplatePromise = new Promise((resolve) => {
                    require.ensure([], () => resolve(require('./skill-editor.html')));
                });

                return (scope, element) => {
                    pGetTemplatePromise
                        .then((template) => {
                            element.html($compile(template)(scope));
                        });
                };
            },
            restrict: 'E',
            transclude: {},
            scope: {
                ngUserId: '<',
                ngSkillId: '<?',
                ngSkillCategoryId: '<?',
                ngPoint: '<',
                ngUser: '<',
                ngSkill: '<',
                ngSkillCategory: '<?',

                ngOnUserSkillUpdate: '&',
                ngOnUserSkillCreate: '&',
                ngOnWindowCancel: '&'
            },
            controller: ($scope,
                         $user, $skill) => {

                //#region Properties

                /*
                * Model for binding information to directive.
                * */
                $scope.oModel = {
                    userId: null,
                    skillId: null,
                    point: 5
                };

                /*
                * List of items that can be displayed on drop-down list.
                * */
                $scope.oItemSource = {
                    users: [],
                    skills: [],
                    skillCategories: []
                };

                //#endregion

                //#region Methods

                /*
                * Called when component is initialized.
                * */
                $scope.ngOnInit = () => {
                    // In edit mode, just use the cache.
                    if ($scope.bIsInEditMode()) {

                        $scope.oModel.userId = $scope.ngUserId;
                        $scope.oModel.skillId = $scope.ngSkillId;
                        if ($scope.ngPoint)
                            $scope.oModel.point = $scope.ngPoint;
                    }

                    if ($scope.ngUser) {
                        $scope.oItemSource.users = [$scope.ngUser];
                        $scope.oModel.userId = $scope.ngUser.id;
                    }

                    if ($scope.ngSkill) {
                        $scope.oItemSource.skills = [$scope.ngSkill];
                        $scope.oModel.skillId = $scope.ngSkill.id;
                    }

                    if ($scope.ngSkillCategory) {
                        $scope.oItemSource.skillCategories = [$scope.ngSkillCategory];
                        $scope.oModel.skillCategoryId = $scope.ngSkillCategory.id;
                    }

                    // In create mode. Load everything.
                    let pLoadUsersPromise = $scope.loadUsers();
                    let pLoadSkillsPromise = $scope.loadSkills();

                    Promise.all([pLoadUsersPromise, pLoadSkillsPromise])
                        .then((pResponses) => {
                            $scope.$applyAsync(() => {
                                $scope.oItemSource.users = pResponses[0] ? pResponses[0].records : null;
                                $scope.oItemSource.skills = pResponses[1] ? pResponses[1].records : null;
                            })
                        });
                };

                /*
                * Check whether edit mode is on or off.
                * */
                $scope.bIsInEditMode = () => {
                    return ($scope.ngUserId || $scope.ngUser) && ($scope.ngSkillId || $scope.ngSkill);
                };

                /*
                * Load user from database.
                * */
                $scope.loadUsers = (bAutoResolve) => {
                    return $user.getUsers()
                        .then((users) => {
                            if (bAutoResolve) {
                                $scope.$applyAsync(() => {
                                    $scope.oItemSource.users = users;
                                });
                            }
                            return users;
                        });
                };

                /*
                * Load skills from database.
                * */
                $scope.loadSkills = (bAutoResolve) => {
                    return $skill
                        .getSkills()
                        .then((skills) => {
                            if (bAutoResolve) {
                                $scope.$applyAsync(() => {
                                    $scope.oItemSource.skills = skills;
                                });
                            }

                            return skills;
                        });
                };

                /*
                * Callback which is raised when ok button is clicked.
                * */
                $scope.clickOk = ($event) => {

                    if ($event)
                        $event.preventDefault();

                    // In edit-mode.
                    if ($scope.bIsInEditMode()) {
                        $scope.ngOnUserSkillUpdate({
                            info: {
                                skillCategoryId: $scope.oModel.skillCategoryId,
                                userId: $scope.oModel.userId,
                                skillId: $scope.oModel.skillId,
                                point: $scope.oModel.point
                            }
                        });

                        return;
                    }

                    $scope.ngOnUserSkillCreate({
                        info: {
                            skillCategoryId: $scope.oModel.skillCategoryId,
                            userId: $scope.oModel.userId,
                            skillId: $scope.oModel.skillId,
                            point: $scope.oModel.point
                        }
                    });
                };

                /*
                * Callback which is raised when cancel button is clicked.
                * */
                $scope.clickCancel = () => {
                    $scope.ngOnWindowCancel();
                };

                //#endregion

            }
        }
    });
};