module.exports = (ngModule) => {
    // Directive declaration.
    ngModule.directive('projectEditor', ($compile) => {
        return {
            compile: () => {
                let pGetTemplatePromise = new Promise((resolve) => {
                    require.ensure([], () => resolve(require('./project-editor.html')));
                });

                return (scope, element) => {
                    pGetTemplatePromise
                        .then((template) => {
                            element.html($compile(template)(scope));
                        });
                };
            },
            restrict: 'E',
            scope: {
                ngOnSkillsLoad: '&',
                ngOnProjectLoad: '&',
                ngOnResponsibilitiesLoad: '&',

                ngOnProjectCreate: '&',
                ngOnProjectEdit: '&',
                ngOnWindowCancel: '&'
            },
            controller: (moment,
                         $ui,
                         $project, $responsibility, $skill, $scope) => {

                //#region Properties

                // Item sources.
                $scope.oItemSource = {
                    responsibilities: [],
                    skills: []
                };

                // Editor option.
                $scope.oEditorOption = {
                    height: 300,
                    focus: true,
                    toolbar: [
                        ['style', ['bold', 'italic', 'underline', 'clear']],
                        ['fontsize', ['fontsize']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['insert', ['link', 'picture']]
                    ]
                };

                // Model for binding.
                $scope.oModel = {
                    projectId: null,
                    name: null,
                    startedTime: null,
                    finishedTime: null,
                    description: null,
                    responsibilityIds: [],
                    skillIds: []
                };

                // Item which will be added to model queue.
                $scope.oItemQueue = {
                    responsibilityId: null,
                    skillId: null
                };

                //#endregion

                //#region Methods

                /*
                * Load projects from database.
                * */
                $scope.loadProject = () => {

                    let pLoadProjectsPromise = null;
                    if (!$scope.ngOnProjectLoad) {
                        pLoadProjectsPromise = new Promise(resolve => {
                            resolve(null);
                        });
                    } else {
                        pLoadProjectsPromise = new Promise(resolve => {
                            resolve($scope.ngOnProjectLoad());
                        });
                    }

                    return pLoadProjectsPromise
                        .catch(() => {
                            return null;
                        });
                };

                /*
                * Load responsibilities from database.
                * */
                $scope.loadResponsibilities = () => {

                    let pLoadResponsibilitiesPromise = null;
                    if (!$scope.ngOnResponsibilitiesLoad) {
                        pLoadResponsibilitiesPromise = new Promise(resolve => {
                            resolve([]);
                        });
                    } else {
                        pLoadResponsibilitiesPromise = new Promise(resolve => {
                            resolve($scope.ngOnResponsibilitiesLoad());
                        })
                    }

                    return pLoadResponsibilitiesPromise
                        .catch((error) => {
                            return [];
                        });
                };

                /*
                * Load skills from database.
                * */
                $scope.loadSkills = () => {

                    let pLoadSkillsPromise = null;
                    if (!$scope.ngOnSkillsLoad) {
                        return new Promise((resolve, reject) => {
                            resolve([]);
                        });
                    } else {
                        pLoadSkillsPromise = new Promise((resolve) => {
                            resolve($scope.ngOnSkillsLoad());
                        });
                    }

                    return pLoadSkillsPromise
                        .catch(() => {
                            return [];
                        });
                };

                /*
                * Called when directive is initialized.
                * */
                $scope.ngOnInit = () => {
                    // Load items source.
                    let pLoadProjectPromise = $scope.loadProject();
                    let pLoadResponsibilitiesPromise = $scope.loadResponsibilities();
                    let pLoadSkillsPromise = $scope.loadSkills();

                    // Block app ui.
                    $ui.blockAppUI();

                    Promise
                        .all([pLoadProjectPromise, pLoadResponsibilitiesPromise, pLoadSkillsPromise])
                        .then((pPromiseResponses) => {

                            let project = pPromiseResponses[0];
                            $scope.oItemSource.responsibilities = pPromiseResponses[1];
                            $scope.oItemSource.skills = pPromiseResponses[2];

                            // Project id is specified. Bind the project to model.

                            if (project) {
                                angular.copy(project, $scope.oModel);
                                $scope.oModel.id = project.id;
                                $scope.oModel.startedTime = new Date($scope.oModel.startedTime);
                                $scope.oModel.finishedTime = new Date($scope.oModel.finishedTime);

                                if (project.projectResponsibilities && project.projectResponsibilities.length) {
                                    $scope.oModel.responsibilityIds = project
                                        .projectResponsibilities
                                        .map((item) => {
                                            return item.responsibilityId;
                                        });
                                }

                                if (project.projectSkills && project.projectSkills.length) {
                                    $scope.oModel.skillIds = project
                                        .projectSkills
                                        .map((item) => {
                                            return item.skillId;
                                        });
                                }
                            }
                        })
                        .catch(() => {
                            return null;
                        })
                        .then(() => {
                            // Update items list.
                            $scope.$applyAsync();

                            // Unblock UI.
                            $ui.unblockAppUI();
                        });
                };

                /*
                * Add responsibility to queue.
                * */
                $scope.addResponsibility = (responsibilityId) => {

                    // Check whether the existing list contain this id or not.
                    if ($scope.oModel.responsibilityIds.indexOf(responsibilityId) !== -1)
                        return;

                    $scope.oModel.responsibilityIds.push(responsibilityId);
                    $scope.oItemQueue.responsibilityId = null;
                };

                /*
                * Add technology to queue.
                * */
                $scope.addSkill = (skillId) => {
                    // Check whether the existing list contain this id or not.
                    if ($scope.oModel.skillIds.indexOf(skillId) !== -1)
                        return;

                    $scope.oModel.skillIds.push(skillId);
                    $scope.oItemQueue.skillId = null;
                };

                /*
                * Called when a project is confirmed to be created | edited.
                * */
                $scope.clickOk = ($event) => {

                    if ($event)
                        $event.preventDefault();

                    if ($scope.addProjectForm.$invalid)
                        return;

                    let model = angular.copy($scope.oModel);
                    model.startedTime = new Date(model.startedTime).getTime();
                    model.finishedTime = new Date(model.finishedTime).getTime();

                    // In create mode.
                    if (!$scope.oModel.id) {
                        $scope.ngOnProjectCreate({
                            info: model
                        });

                        return;
                    }

                    $scope.ngOnProjectEdit({
                        info: model
                    });
                };

                /*
                * Called when cancel button is clicked.
                * */
                $scope.clickCancel = () => {
                    $scope.ngOnWindowCancel();
                };

                /*
                * Check whether start time is valid or not.
                * */
                $scope.bIsValidStartTime = (startedTime, finishedTime) => {
                    let oStartedTime = new Date(startedTime);
                    if (!oStartedTime)
                        return false;

                    let oFinishedTime = new Date(finishedTime);
                    return (oStartedTime < oFinishedTime);
                };

                /*
                * Check whether finished time is valid or not.
                * */
                $scope.bIsValidFinishTime = (startedTime, finishedTime) => {
                    let oStartedTime = new Date(startedTime);
                    let oFinishedTime = new Date(finishedTime);
                    return (oStartedTime < oFinishedTime);
                };

                /*
                * Delete project technology from list.
                * */
                $scope.deleteProjectSkill = (skillId) => {
                    // Find item id.
                    let itemId = $scope.oModel.skillIds.indexOf(skillId);
                    if (itemId === -1)
                        return;

                    $scope.oModel.skillIds.splice(itemId, 1);
                };

                $scope.deleteProjectResponsibility = (projectResponsibilityId) => {

                    // Find item id.
                    let itemId = $scope.oModel.responsibilityIds.indexOf(projectResponsibilityId);
                    if (itemId === -1)
                        return;

                    $scope.oModel.responsibilityIds.splice(itemId, 1);
                };

                //#endregion
            }
        }
    });
};