module.exports = (ngModule) => {
    // Directive declaration.
    ngModule.directive('userPicker', ($compile) => {
        return {
            compile: () => {
                let pGetTemplatePromise = new Promise((resolve) => {
                    require.ensure([], () => resolve(require('./user-picker.html')));
                });

                return (scope, element) => {
                    pGetTemplatePromise
                        .then((template) => {
                            element.html($compile(template)(scope));
                        });
                };
            },
            restrict: 'E',
            transclude: {},
            scope: {
                ngOnSelectUser: '&',
                ngOnClose: '&'
            },
            controller: function ($scope, $user) {

                //#region Properties

                // Model to store query result.
                $scope.oResultModel = {
                    user: {
                        records: null,
                        total: 0
                    }
                };

                //#endregion

                //#region Methods

                /*
                * Called when directive is initialized.
                * */
                $scope.ngOnInit = () => {
                    $user.getUsers(null)
                        .then((result) => {
                            $scope.$apply(() => {
                                $scope.oResultModel.user = result;
                            });
                        })
                };

                /*
                * Called when picker close button is clicked.
                * */
                $scope.closePicker = () => {
                    $scope.ngOnClose();
                };

                /*
                * Called when an user is selected.
                * */
                $scope.selectUser = (user) => {
                    $scope.ngOnSelectUser({user: user});
                }

                //#endregion
            }
        }
    });
};