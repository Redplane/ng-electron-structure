module.exports = (ngModule) => {
    require('./side-bar/side-bar')(ngModule);
    require('./navigation-bar/navigation-bar')(ngModule);
    require('./user-picker/user-picker')(ngModule);
    require('./skill-editor/skill-editor')(ngModule);
    require('./project-editor/project-editor')(ngModule);
    require('./image-uploader/image-uploader')(ngModule);
    require('./skill-category-editor/skill-category-editor')(ngModule);
};