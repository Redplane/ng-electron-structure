module.exports = (ngModule) => {

    // Import style.
    require('./skill-category-editor.scss');

    // Directive declaration.
    ngModule.directive('skillCategoryEditor', ($compile) => {
        return {
            compile: () => {
                let pGetTemplatePromise = new Promise((resolve) => {
                    require.ensure([], () => resolve(require('./skill-category-editor.html')));
                });

                return (scope, element) => {
                    pGetTemplatePromise
                        .then((template) => {
                            element.html($compile(template)(scope));
                        });
                };
            },
            restrict: 'E',
            scope: {
                ngLoadSkillCategory: '&',
                ngOnCancel: '&',
                ngOnSelect: '&',
                ngOnEdit: '&'
            },
            controller: (moment, FileUploader,
                         $ui,
                         $scope) => {

                //#region Properties

                // Model for 2 way data binding.
                $scope.oModel = {
                    id: null,
                    userId: null,
                    name: null,
                    base64CroppedImage: null,
                    croppedImage: null
                };

                // Skill category image uploader.
                $scope._skillCategoryImageUploader = new FileUploader({});


                //#endregion

                //#region Methods

                /*
                * Called when cancel button is clicked.
                * */
                $scope.clickCancel = () => {
                    if ($scope.ngOnCancel)
                        $scope.ngOnCancel();
                };

                /*
                * Called when select button (OK) is clicked.
                * */
                $scope.selectSkillCategory = ($event) => {

                    // Prevent default behaviour.
                    if ($event)
                        $event.preventDefault();

                    if (!$scope.skillCategoryEditor)
                        return;

                    if (!$scope.skillCategoryEditor.$valid)
                        return;

                    // Find model.
                    let model = $scope.oModel;

                    if (!$scope.oModel.id) {
                        $scope
                            .ngOnSelect({
                                userId: model.userId,
                                name: model.name,
                                encodedPhoto: model.base64CroppedImage,
                                binaryPhoto: model.croppedImage
                            });

                        return;
                    }


                    $scope.ngOnEdit({
                        id: model.id,
                        userId: model.userId,
                        name: model.name,
                        encodedPhoto: model.base64CroppedImage,
                        binaryPhoto: model.croppedImage
                    });
                };

                /*
                * Called when directive template is initialized.
                * */
                $scope.ngOnInit = () => {


                    // Load skill category information.
                    let pLoadSkillCategoryPromise = null;
                    if ($scope.ngLoadSkillCategory)
                        pLoadSkillCategoryPromise = new Promise((resolve) => {
                            resolve($scope.ngLoadSkillCategory());
                        });
                    else {
                        pLoadSkillCategoryPromise = new Promise((resolve) => {
                            resolve({
                                id: null,
                                userId: null,
                                photo: null,
                                name: null,
                                createdTime: 0
                            })
                        });
                    }

                    pLoadSkillCategoryPromise
                        .then((skillCategory) => {
                            $scope.oModel.id = skillCategory.id;
                            $scope.oModel.userId = skillCategory.userId;
                            $scope.oModel.name = skillCategory.name;
                            $scope.oModel.encodedPhoto = skillCategory.photo;
                            $scope.oModel.name = skillCategory.name;
                            $scope.oModel.createdTime = skillCategory.createdTime;

                            $scope.$applyAsync();
                        });
                };

                //#endregion

                //#region Events

                /*
                * Called when a file is selected.
                * */
                $scope._skillCategoryImageUploader.onAfterAddingFile = (fileItem) => {
                    // Read the selected file.
                    let file = fileItem._file;
                    $scope._selectedImage = file;
                };


                //#endregion
            }
        }
    });
};