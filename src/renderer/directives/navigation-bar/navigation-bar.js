module.exports = (ngModule) => {

    // Module template import.
    const ngModuleHtmlTemplate = require('./navigation-bar.html');

    // Directive declaration.
    ngModule.directive('navigationBar', () => {
        return {
            template: ngModuleHtmlTemplate,
            restrict: 'E',
            transclude: {},
            scope: {},
            controller: function ($ui,
                                  $element, $window,
                                  $scope) {

                //#region Methods

                /*
                * Called when component is initialized.
                * */
                $scope.ngOnInit = () => {

                };

                /*
                * Called when toggle sidebar button is clicked.
                * */
                $scope.toggleSidebar = () => {
                    if ($window.outerWidth > 1194) {
                        $ui.findDomElement('nav.side-navbar').toggleClass('shrink');
                        $ui.findDomElement('.page').toggleClass('active');
                        return;
                    }

                    $ui.findDomElement('nav.side-navbar').toggleClass('show-sm');
                    $ui.findDomElement('.page').toggleClass('active-sm');
                };

                //#endregion
            }
        }
    });
};