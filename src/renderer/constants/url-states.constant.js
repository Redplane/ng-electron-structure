module.exports = (ngModule) => {

    /*
    * Application constants declaration.
    * */
    ngModule.constant('urlStatesConstant', {
        login: {
            url: '/login',
            name: 'login'
        },

        technicalSkill: {
            url: '/technical-skill',
            name: 'technical-skill'
        },

        userProject: {
            url: '/user-project',
            name: 'user-project'
        },

        dashboard: {
            url: '/dashboard',
            name: 'dashboard'
        },

        shared: {
            masterLayout: {
                name: 'app-master-layout',
                url: null
            }
        },

        management: {
            skillManagement: {
                name: 'skill-management',
                url: '/skill'
            },
            responsibilityManagement: {
                name: 'responsibility-management',
                url: '/responsibility'
            },
            technologyManagement: {
                name: 'technology-management',
                url: '/technology'
            },
            masterLayout: {
                name: 'management-master-layout',
                url: null
            }
        }
    });
};