module.exports = (ngModule) => {
    ngModule.constant('repositoryConstant', {
        personalSkill: 'PersonalSkill',
        project: 'Project',
        projectResponsibility: 'ProjectResponsibility',
        projectSkill: 'ProjectSkill',
        skill: 'Skill',
        skillCategory: 'SkillCategory',
        technology: 'Technology',
        user: 'User',
        userDescription: 'UserDescription',
        responsibility: 'Responsibility'
    });
};