module.exports = () => {
    require('./user');
    require('./user-decription');
    require('./skill-category');
    require('./project');
    require('./project-skill');
};
