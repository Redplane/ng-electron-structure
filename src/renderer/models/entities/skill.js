module.exports = {
    name: "Skill",
    tableName: 'Skill',
    columns: {
        id: {
            primary: true,
            type: 'int',
            generated: 'increment'
        },
        name: {
            type: 'nvarchar',
            nullable: false
        },
        createdTime: {
            type: 'float',
            nullable: false
        }
    },
    relations:{
        personalSkills:{
            target: "PersonalSkill",
            type: "one-to-many",
            joinColumn: {
                name: 'id',
                referencedColumnName: 'skillId'
            },
            inverseSide: 'skills'
        },
        projectSkills: {
            target: 'ProjectSkill',
            type: 'one-to-many',
            joinColumn: {
                name: 'id',
                referencedColumnName: 'skillId'
            },
            inverseSide: 'projectSkills'
        }
    }
};