module.exports = {
    name: "Project",
    tableName: 'Project',
    columns: {
        id: {
            primary: true,
            type: 'int',
            generated: 'increment'
        },
        userId: {
            type: 'int',
            nullable: false
        },
        name: {
            type: 'nvarchar',
            nullable: false
        },
        description: {
            type: 'nvarchar',
            nullable: false
        },
        startedTime:{
            type: 'float',
            nullable: false
        },
        finishedTime:{
            type: 'float',
            nullable: true
        }
    },
    relations: {
        projectSkills:{
            target: 'ProjectSkill',
            type: 'one-to-many',
            joinColumn: {
                name: 'id',
                referencedColumnName: 'projectId'
            },
            inverseSide: 'project',
            cascade: true
        },
        projectResponsibilities:{
            target: 'ProjectResponsibility',
            type: 'one-to-many',
            joinColumn:{
                name: 'id',
                referencedColumnName: 'projectId'
            },
            inverseSide: 'project',
            cascade: true
        }
    }
};