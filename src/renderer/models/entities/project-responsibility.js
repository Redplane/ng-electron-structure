module.exports = {
    name: 'ProjectResponsibility',
    tableName: 'ProjectResponsibility',
    columns: {
        projectId: {
            type: 'int',
            primary: true
        },
        responsibilityId: {
            type: 'int',
            primary: true
        }
    },
    relations: {
        project: {
            target: 'Project',
            type: 'many-to-one',
            joinColumn: {
                name: 'projectId',
                referencedColumnName: 'id'
            },
            inverseSide: 'projectResponsibilities'
        },
        responsibility: {
            target: 'Responsibility',
            type: 'many-to-one',
            joinColumn:{
                name: 'responsibilityId',
                referencedColumnName: 'id'
            },
            inverseSide: 'projectResponsibilities'
        }
    }
};