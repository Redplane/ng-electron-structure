module.exports = {
    name: "PersonalSkill",
    tableName: 'PersonalSkill',
    columns: {
        skillCategoryId: {
            primary: true,
            type: 'int'
        },
        skillId: {
            primary: true,
            type: 'int'
        },
        point: {
            type: 'int',
            nullable: false
        },
        createdTime: {
            type: 'float',
            nullable: false
        }
    },
    relations: {
        skill: {
            target: "Skill",
            type: "many-to-one",
            joinColumn: {
                name: 'skillId',
                referencedColumnName: 'id'
            },
            inverseSide: 'personalSkills'
        },
        skillCategory: {
            target: 'SkillCategory',
            type: 'many-to-one',
            joinColumn: {
                name: 'skillCategoryId',
                referencedColumnName: 'id'
            },
            inverseSide: 'personalSkills'
        }
    }
};