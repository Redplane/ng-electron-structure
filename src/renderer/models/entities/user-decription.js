module.exports = {
    name: "UserDescription",
    tableName: 'UserDescription',
    columns: {
        id: {
            primary: true,
            type: 'int',
            generated: 'increment'
        },
        userId:{
            type: 'int',
            nullable: false
        },
        description: {
            type: 'nvarchar',
            nullable: false
        }
    }
};