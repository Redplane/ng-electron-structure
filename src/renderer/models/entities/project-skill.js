module.exports = {
    name: '',
    tableName: 'ProjectSkill',
    columns: {
        projectId: {
            type: 'int',
            primary: true
        },
        skillId: {
            type: 'int',
            primary: true
        }
    },
    relations: {
        project: {
            target: 'Project',
            type: 'many-to-one',
            joinColumn: {
                name: 'projectId',
                referencedColumnName: 'id'
            },
            inverseSide: 'projectTechnologies'
        },
        skill: {
            target: 'Skill',
            type: 'many-to-one',
            joinColumn:{
                name: 'skillId',
                referencedColumnName: 'id'
            },
            inverseSide: 'projectTechnologies'
        }
    }
};