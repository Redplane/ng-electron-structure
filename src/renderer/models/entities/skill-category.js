module.exports = {
    name: "SkillCategory",
    tableName: 'SkillCategory',
    columns: {
        id: {
            primary: true,
            type: 'int',
            generated: 'increment'
        },
        userId: {
            type: 'int',
            nullable: false
        },
        photo: {
            type: 'nvarchar',
            nullable: false
        },
        name: {
            type: 'nvarchar',
            nullable: false
        },
        createdTime: {
            type: 'float',
            nullable: false
        }
    },
    relations: {
        personalSkills: {
            target: 'PersonalSkill',
            type: 'one-to-many',
            joinColumn: {
                name: 'id',
                referencedColumnName: 'skillCategoryId'
            },
            inverseSide: 'skillCategory'
        }
    }
};