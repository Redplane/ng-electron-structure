module.exports = {
    name: 'Responsibility',
    tableName: 'Responsibility',
    columns: {
        id: {
            primary: true,
            type: 'int',
            generated: 'increment'
        },
        name: {
            type: 'nvarchar',
            nullable: false,
        },
        createdTime:{
            type: 'nvarchar',
            nullable: false
        },
        lastModifiedTime: {
            type: 'float',
            nullable: true
        }
    },
    relations: {
        projectResponsibilities:{
            target: 'ProjectResponsibility',
            type: 'one-to-many',
            joinColumn: {
                name: 'id',
                referencedColumnName: 'responsibilityId'
            },
            inverseSide: 'responsibility'
        }
    }
};