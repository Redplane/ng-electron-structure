module.exports = {
    name: "User",
    columns: {
        id: {
            primary: true,
            type: "int",
            generated: 'increment'
        },
        firstName: {
            type: 'nvarchar',
            nullable: false
        },
        lastName:{
            type: 'nvarchar',
            nullable: false
        },
        photo: {
            type: 'nvarchar',
            nullable: true
        },
        birthday: {
            type: 'float',
            nullable: 0
        },
        role: {
            type: 'nvarchar',
            nullable: false
        }
    }
};