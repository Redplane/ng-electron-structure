module.exports = (ngModule) => {
    require('./app.css');

    require('./dashboard')(ngModule);
    require('./technical-skill')(ngModule);
    require('./user-project')(ngModule);
    require('./management')(ngModule);
    require('./shared')(ngModule);
};