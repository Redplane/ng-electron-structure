// Import module template.
module.exports = (ngModule) => {
    ngModule.config((
        urlStatesConstant,
        $stateProvider) => {

        //#region State registration

        const urlStateHome = urlStatesConstant.dashboard;
        const urlStateUserProject = urlStatesConstant.userProject;

        $stateProvider.state(urlStateUserProject.name, {
            url: urlStateUserProject.url,
            parent: urlStateHome.name,
            templateProvider: ($q) => {
                return $q((resolve) => {
                    // lazy load the view
                    require.ensure([], () => resolve(require('./user-project.html')));
                });
            },
            controller: 'userProjectController',
            resolve: {
                loadUserProjectController: ($q, $ocLazyLoad) => {
                    return $q((resolve) => {
                        require.ensure([], () => {
                            // load only controller module
                            let module = angular.module('user-project', []);
                            require('./user-project.controller')(module);
                            $ocLazyLoad.load({name: module.name});
                            resolve(module.controller);
                        })
                    });
                },

                user: ($appCache, $state, urlStatesConstant, cacheKeyConstant) => {
                    let user = $appCache.get(cacheKeyConstant.user);
                    if (!user){
                        // Redirect back to main page.
                        $state.go(urlStatesConstant.dashboard.name);
                        throw 'No user information is found';
                    }

                    return user;
                }
            }
        })

        //#endregion
    });
};