module.exports = (ngModule) => {
    ngModule.controller('userProjectController', (user,
                                                  $project, $skill, $responsibility,
                                                  $ui, $uibModal, $translate,
                                                  toastr,
                                                  $scope) => {

        //#region Properties

        // List of user projects.
        $scope.userProjects = [];

        // List of screen modal dialog instances.
        $scope.oModals = {
            addProject: {},
            editProject: {}
        };

        // Model for information binding.
        $scope.oModel = {
            editProject: {
                projectId: null
            }
        };
        //#endregion

        //#region Methods

        /*
        * Called when component is initialized.
        * */
        $scope.ngOnInit = () => {

            // Block UI.
            $ui.blockAppUI();

            $project.loadProjects({userIds: [user.id]})
                .then((result) => {
                    $scope.$apply(() => {
                        $scope.userProjects = result.records;
                        $ui.unblockAppUI();
                    });
                });
        };

        /*
        * Display project initiator.
        * */
        $scope.displayProjectInitiator = () => {

            // Reset project id.
            $scope.oModel.editProject.projectId = null;

            // Display project initiator modal.
            $scope.oModals.addProject = $uibModal.open({
                templateUrl: 'project-initiator.html',
                scope: $scope,
                size: 'lg'
            });
        };

        /*
        * Display project editor.
        * */
        $scope.displayProjectEditor = (projectId) => {
            if (!projectId)
                return;

            $scope.oModel.editProject.projectId = projectId;

            // Display project initiator modal.
            $scope.oModals.editProject = $uibModal.open({
                templateUrl: 'project-editor.html',
                scope: $scope,
                size: 'lg'
            });
        };

        /*
        * Called when a project is created.
        * */
        $scope.ngOnProjectCreate = (info) => {

            // Block app ui.
            $ui.blockAppUI();
            $project.addProject(user.id, info.name, info.description, info.startedTime, info.finishedTime, info.responsibilityIds, info.skillIds)
                .then(() => {

                    let message = $translate.instant('MSG_ADD_PROJECT_SUCCESS');
                    toastr.success(message);

                    // Close modal.
                    if ($scope.oModals.addProject) {
                        $scope.oModals.addProject.dismiss();
                        $scope.oModals.addProject = null;
                    }
                    $scope.ngOnInit();
                })
                .catch((error) => {

                    let message = $translate.instant('MSG_ADD_PROJECT_FAIL');
                    toastr.success(message);
                    $scope.$applyAsync(() => {
                        $ui.unblockAppUI();
                    });
                });
        };

        /*
        * Called when project is edited.
        * */
        $scope.ngOnProjectEdit = (info) => {
            // Block app ui.
            $ui.blockAppUI();

            $project.editProject(info.id, info.name, info.description, info.startedTime, info.finishedTime, info.responsibilityIds, info.technologyIds)
                .then(() => {

                    let message = $translate.instant('MSG_EDIT_PROJECT_SUCCESS');
                    toastr.success(message);

                    // Close modal.
                    if ($scope.oModals.editProject) {
                        $scope.oModals.editProject.dismiss();
                        $scope.oModals.editProject = null;
                    }
                    $scope.ngOnInit();
                })
                .catch((error) => {

                    let message = $translate.instant('MSG_EDIT_PROJECT_FAIL');
                    toastr.error(message);

                    console.log(error);
                    $scope.$applyAsync(() => {
                        $ui.unblockAppUI();
                    });
                });
        };

        //#endregion

        //#region Events

        /*
        * Called when project is loaded.
        * */
        $scope.ngOnProjectLoad = () => {
            let editProject = $scope.oModel.editProject;

            // In create-mode.
            if (!editProject.projectId) {
                return null;
            }

            return $project
                .loadProjects({
                    ids: editProject.projectId
                })
                .then((loadProjectsResult) => {
                    let projects = loadProjectsResult.records;
                    if (!projects || !projects.length)
                        return null;

                    return projects[0];
                });
        };

        /*
        * Called when skills are requested to be loaded.
        * */
        $scope.ngOnSkillsLoad = () => {
            return $skill
                .getSkills()
                .then((loadSkillsResult) => {
                    console.log(loadSkillsResult);
                    return loadSkillsResult.records;
                });

        };

        /*
        * Called when responsibilities are requested to be loaded.
        * */
        $scope.ngOnResponsibilitiesLoad = () => {
            return $responsibility
                .loadResponsibilities()
                .then((loadResponsibilitiesResult) => {
                    return loadResponsibilitiesResult.records;
                });

        };

        //#endregion
    });
};