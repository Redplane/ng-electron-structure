module.exports = (ngModule) => {

    // Import template.
    const ngModuleHtmlTemplate = require('./skill-management.html');

    ngModule.config(($stateProvider, urlStatesConstant) => {

        //#region State registration

        const urlStateManagement = urlStatesConstant.management;
        const urlStateManagementMasterLayout = urlStateManagement.masterLayout;
        const urlStateSkillManagement = urlStateManagement.skillManagement;

        $stateProvider
            .state(urlStateSkillManagement.name, {
                url: urlStateSkillManagement.url,
                parent: urlStateManagementMasterLayout.name,
                templateProvider: ($q) => {
                    return $q((resolve) => {
                        // lazy load the view
                        require.ensure([], () => resolve(require('./skill-management.html')));
                    });
                },
                controller: 'skillManagementController',
                resolve: {
                    loadSkillManagementController: ($q, $ocLazyLoad) => {
                        return $q((resolve) => {
                            require.ensure([], () => {
                                // load only controller module
                                let module = angular.module('management.skill', []);
                                require('./skill-management.controller')(module);
                                $ocLazyLoad.load({name: module.name});
                                resolve(module.controller);
                            })
                        });
                    }
                }
            });

        //#endregion

    });
};