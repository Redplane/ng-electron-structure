module.exports = (ngModule) => {
    ngModule.controller('skillManagementController', ($scope,
                                                      $translate,
                                                      $ngConfirm,
                                                      $skill, $ui) => {

        //#region Properties

        /*
        * Source of item.
        * */
        $scope.oItemSource = {
            skills: []
        };

        // Model for information binding.
        $scope.oModel = {
            addSkill: {
                name: ''
            },
            editSkill: {
                id: null,
                name: null
            }
        };

        //#endregion

        //#region Methods

        /*
        * Called when component is initialized.
        * */
        $scope.ngOnInit = () => {

            // Block app UI.
            $ui.blockAppUI();

            // Load skills.
            $scope.loadSkills()
                .then((result) => {
                    $scope.$applyAsync(() => {
                        $scope.oItemSource.skills = result.records;
                        $ui.unblockAppUI();
                    });
                });
        };

        /*
        * Add skill by using specific information.
        * */
        $scope.addSkill = ($event) => {

            if ($event)
                $event.preventDefault();

            if (!$scope.addSkillForm || $scope.addSkillForm.$invalid)
                return;

            let model = {};
            angular.copy($scope.oModel.addSkill, model);

            // Block app ui.
            $ui.blockAppUI();

            $skill.addSkill(model.name)
                .then(() => {
                    return $scope.loadSkills();
                })
                .then((result) => {
                    $scope.$applyAsync(() => {
                        $scope.oModel.addSkill.name = null;
                        $scope.oItemSource.skills = result.records;

                        // Mark the control as pristine.
                        $scope.addSkillForm.skillName.$setPristine(true);
                        $ui.unblockAppUI();
                    })
                })
                .catch(() => {
                    $scope.$applyAsync(() => {
                        $ui.unblockAppUI();
                    })
                });
        };

        /*
        * Search and delete skill by using specific conditions.
        * */
        $scope.deleteSkill = (skillId) => {

            $ngConfirm({
                title: '',
                theme: 'bootstrap',
                content: '<b class="text-danger">{{"MSG_CONFIRM_DELETE_SKILL" | translate}}</b>',
                scope: $scope,
                buttons: {
                    ok: {
                        text: $translate.instant('TITLE_OK'),
                        btnClass: 'btn-danger',
                        action: () => {
                            // Block app ui.
                            $ui.blockAppUI();

                            $skill.deleteSkill(skillId)
                                .then(() => {
                                    // Load skills.
                                    return $scope.loadSkills();
                                })
                                .then((result) => {
                                    $scope.$apply(() => {
                                        $scope.oItemSource.skills = result.records;
                                        $ui.unblockAppUI();
                                    });
                                })
                                .catch((error) => {
                                    $ui.unblockAppUI();
                                });
                        }
                    },
                    cancel: {
                        text: $translate.instant('TITLE_CANCEL'),
                        btnClass: 'btn-default',
                        action: (scope, button) => {
                        }
                    }
                }
            });


        };

        /*
        * Edit skill by using specific conditions.
        * */
        $scope.editSkill = () => {

            if (!$scope.oModel.editSkill || !$scope.oModel.editSkill.id)
                return;

            let model = $scope.oModel.editSkill;

            // Block app ui.
            $ui.blockAppUI();

            $skill.editSkill(model.id, model.name)
                .then(() => {
                    return $scope.loadSkills();
                })
                .then((result) => {
                    $scope.$applyAsync(() => {
                        $scope.oModel.editSkill.id = null;
                        $scope.oItemSource.skills = result.records;

                        // Mark the control as pristine.
                        $scope.addSkillForm.skillName.$setPristine(true);
                        $ui.unblockAppUI();
                    })
                })
                .catch(() => {
                    $scope.$applyAsync(() => {
                        $ui.unblockAppUI();
                    })
                });

        };

        /*
        * Enable skill editor.
        * */
        $scope.enableSkillEditor = (skill) => {
            $scope.oModel.editSkill.id = skill.id;
            $scope.oModel.editSkill.name = skill.name;
        };

        /*
        * Cancel skill editor.
        * */
        $scope.cancelSkillEditor = () => {
            $scope.oModel.editSkill.id = null;
        };

        /*
        * Load skills from data source.
        * */
        $scope.loadSkills = () => {
            return $skill.getSkills();
        };


        //#endregion
    });
};