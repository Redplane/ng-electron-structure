module.exports = (ngModule) => {
    ngModule.controller('managementMasterLayoutController', ($scope,
                                                             urlStatesConstant) => {

        //#region Properties

        // Constant reflection.
        $scope.urlStatesConstant = urlStatesConstant;
        $scope.urlStateManagementConstant = urlStatesConstant.management;

        //#endregion

        //#region Methods

        //#endregion
    });
};