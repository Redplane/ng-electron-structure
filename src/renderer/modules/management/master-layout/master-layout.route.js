module.exports = (ngModule) => {
    ngModule.config(($stateProvider,
                     urlStatesConstant) => {

        //#region States registration

        const urlStateManagement = urlStatesConstant.management;
        const urlStateSkillManagement = urlStateManagement.skillManagement;
        const urlManagementMasterLayout = urlStateManagement.masterLayout;
        const urlStateShared = urlStatesConstant.shared;

        $stateProvider.state(urlManagementMasterLayout.name, {
            url: null,
            parent: urlStateShared.masterLayout.name,
            controller: 'managementMasterLayoutController',
            templateProvider: ($q) => {
                return $q((resolve) => {
                    // lazy load the view
                    require.ensure([], () => resolve(require('./master-layout.html')));
                });
            },
            redirectTo: urlStateSkillManagement.name,
            resolve: {
                loadManagementMasterLayoutController: ($q, $ocLazyLoad) => {
                    return $q((resolve) => {
                        require.ensure([], () => {
                            // load only controller module
                            let module = angular.module('management.master-layout', []);
                            require('./master-layout.controller')(module);
                            $ocLazyLoad.load({name: module.name});
                            resolve(module.controller);
                        })
                    });
                }
            }
        });

        //#endregion
    });
};