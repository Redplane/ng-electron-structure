module.exports = (ngModule) => {
    require('./master-layout')(ngModule);
    require('./responsibility-management')(ngModule);
    require('./skill-management')(ngModule);
};