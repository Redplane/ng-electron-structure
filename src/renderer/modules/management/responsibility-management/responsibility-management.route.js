module.exports = (ngModule) => {
    ngModule
        .config(($stateProvider, urlStatesConstant) => {

            //#region State registration

            const urlStateManagement = urlStatesConstant.management;
            const urlStateManagementMasterLayout = urlStateManagement.masterLayout;
            const urlStateManagementResponsibility = urlStateManagement.responsibilityManagement;

            $stateProvider.state(urlStateManagementResponsibility.name, {
                url: urlStateManagementResponsibility.url,
                parent: urlStateManagementMasterLayout.name,
                controller: 'responsibilityManagementController',
                templateProvider: ($q) => {
                    return $q((resolve) => {
                        // lazy load the view
                        require.ensure([], () => resolve(require('./responsibility-management.html')));
                    });
                },
                resolve:{
                    loadResponsibilityManagementController: ($q, $ocLazyLoad) => {
                        return $q((resolve) => {
                            require.ensure([], () => {
                                // load only controller module
                                let module = angular.module('management.responsibility', []);
                                require('./responsibility-management.controller')(module);
                                $ocLazyLoad.load({name: module.name});
                                resolve(module.controller);
                            })
                        });
                    }
                }
            });

            //#endregion
        });
};