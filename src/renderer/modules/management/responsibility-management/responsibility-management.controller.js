const _ = require('lodash');

module.exports = (ngModule) => {
    ngModule.controller('responsibilityManagementController', (
        $responsibility,
        $state,
        $ui, $ngConfirm, $translate,
        $scope) => {

        //#region Properties

        // Items source.
        $scope.oItemSource = {
            responsibilities: []
        };

        // Model for information binding.
        $scope.oModel = {
            addResponsibility: {
                name: null
            },
            editResponsibility: {
                id: null,
                name: null
            }
        };

        //#endregion

        //#region Methods

        /*
        * Called when component is initialized.
        * */
        $scope.ngOnInit = () => {

            // Block app ui.
            $ui.blockAppUI();

            // Load responsibilities.
            $responsibility
                .loadResponsibilities()
                .then((result) => {
                    $scope.oItemSource.responsibilities = result.records;
                    $ui.unblockAppUI();
                    $scope.$applyAsync();

                    return result;
                })
                .catch(() => {
                    $scope.$applyAsync(() => {
                        $ui.blockAppUI();
                    });

                })
        };

        /*
        * Enable responsibility inline editor.
        * */
        $scope.enableResponsibilityEditor = (responsibility) => {
            $scope.$applyAsync(() => {
                let model = $scope.oModel.editResponsibility;
                model.id = responsibility.id;
                model.name = responsibility.name;
            });
        };

        /*
        * Edit responsibility by using specific information.
        * */
        $scope.editResponsibility = () => {
            let model = {};
            angular.copy($scope.oModel.editResponsibility, model);

            $ui.blockAppUI();

            $responsibility
                .editResponsibility(model.id, model.name)
                .then((editedResponsibility) => {
                    let iIndex = _
                        .findIndex($scope.oItemSource.responsibilities, (responsibility) => {
                            return responsibility.id === editedResponsibility.id;
                        });

                    if (iIndex === -1)
                        return null;

                    $scope.oItemSource.responsibilities[iIndex] = editedResponsibility;
                    $scope.oModel.editResponsibility.id = null;
                    $ui.unblockAppUI();
                    $scope.$applyAsync();
                })
                .catch((error) => {
                    console.log(error);
                    $scope.$applyAsync(() => {
                        $ui.unblockAppUI();
                    });
                });
        };

        /*
        * Cancel responsibility edit.
        * */
        $scope.cancelResponsibilityEdit = () => {
            $scope.$applyAsync(() => {
                $scope.oModel.editResponsibility.id = null;
            });
        };

        /*
        * Add a new responsibility to database.
        * */
        $scope.addResponsibility = ($event) => {

            if ($event)
                $event.preventDefault();

            let model = {};
            angular.copy($scope.oModel.addResponsibility, model);

            // Block app ui.
            $ui.blockAppUI();

            $responsibility.addResponsibility(model.name)
                .then(() => {
                    return $responsibility.getResponsibilities();
                })
                .then((loadResponsibilitiesResult) => {
                    $scope.oItemSource.responsibilities = loadResponsibilitiesResult.records;
                })
                .catch(() => {
                    return null;
                })
                .then(() => {
                    $ui.unblockAppUI();
                    $scope.$applyAsync();
                });

        };

        /*
        * Search and delete skill by using specific conditions.
        * */
        $scope.deleteResponsibility = (responsibilityId) => {

            $ngConfirm({
                title: '',
                theme: 'bootstrap',
                content: '<b class="text-danger">{{"MSG_CONFIRM_DELETE_RESPONSIBILITY" | translate}}</b>',
                scope: $scope,
                buttons: {
                    ok: {
                        text: $translate.instant('TITLE_OK'),
                        btnClass: 'btn-danger',
                        action: () => {
                            // Block app ui.
                            $ui.blockAppUI();

                            $responsibility.deleteResponsibility(responsibilityId)
                                .then(() => {
                                    // Load responsibilities.
                                    return $responsibility.loadResponsibilities();
                                })
                                .then((result) => {
                                    $scope.oItemSource.responsibilities = result.records;
                                    $ui.unblockAppUI();
                                    $scope.$applyAsync();
                                })
                                .catch(() => {
                                    $ui.unblockAppUI();
                                    $scope.$applyAsync();
                                });
                        }
                    },
                    cancel: {
                        text: $translate.instant('TITLE_CANCEL'),
                        btnClass: 'btn-default',
                        action: () => {
                        }
                    }
                }
            });

            //#endregion
        }
    })
};