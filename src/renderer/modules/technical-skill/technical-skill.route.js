module.exports = (ngModule) => {
    ngModule.config((
        urlStatesConstant,
        $stateProvider) => {

        //#region State registration

        const urlStateHome = urlStatesConstant.dashboard;
        const urlStateTechnicalSkill = urlStatesConstant.technicalSkill;

        $stateProvider.state(urlStateTechnicalSkill.name, {
            url: urlStateTechnicalSkill.url,
            parent: urlStateHome.name,
            templateProvider: ($q) => {
                return $q((resolve) => {
                    // lazy load the view
                    require.ensure([], () => resolve(require('./technical-skill.html')));
                });
            },
            controller: 'technicalSkillController',
            resolve: {
                loadTechnicalSkillController: ($q, $ocLazyLoad) => {
                    return $q((resolve) => {
                        require.ensure([], () => {
                            // load only controller module
                            let module = angular.module('technical-skill', []);
                            require('./technical-skill.controller')(module);
                            $ocLazyLoad.load({name: module.name});
                            resolve(module.controller);
                        })
                    });
                },

                user: ($appCache, $state, urlStatesConstant, cacheKeyConstant) => {
                    let user = $appCache.get(cacheKeyConstant.user);
                    if (!user){
                        // Redirect back to main page.
                        $state.go(urlStatesConstant.dashboard.name);
                        throw 'No user information is found';
                    }

                    return user;
                }
            }
        })

        //#endregion
    });
};