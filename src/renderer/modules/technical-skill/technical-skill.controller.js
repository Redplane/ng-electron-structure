module.exports = (ngModule) => {

    require('./technical-skill.scss');

    ngModule.controller('technicalSkillController', ($skillCategory, $personalSkill,
                                                     $ui, $uibModal, $state,
                                                     user, $scope) => {

        //#region Properties

        // List of personal techinical skills.
        $scope.technicalSkills = [];

        // Model for data-binding.
        $scope.oModel = {
            editSkill: {
                userId: null,
                skillId: 0,
                skillCategoryId: null,
                point: 0,
                skill: null,
                user: null,
                skillCategory: null
            },
            addSkill: {
                user: null,
                point: 0
            },
            skillCategory: {
                id: null,
                userId: null
            }
        };

        /*
        * List of screen modals.
        * */
        $scope.oModals = {
            skillEditor: {},
            skillInitiator: {},
            skillPhotoPicker: {},
            addSkillCategory: {},
            editSkillCategory: {}
        };

        //#endregion

        //#region Methods

        /*
        * Called when component is initialized.
        * */
        $scope.ngOnInit = () => {

            // Block app UI.
            $ui.blockAppUI();

            // Get skill categories.
            $skillCategory
                .getSkillCategories({userIds: [user.id]})
                .then((result) => {
                    $scope.technicalSkills = result.records;
                    return true;
                })
                .catch(() => {
                    return false;
                })
                .then(() => {
                    $ui.unblockAppUI();
                    $scope.$applyAsync();
                });
        };

        /*
        * Display skill edit.
        * */
        $scope.displayEditSkill = (skill, skillCategory, skillPoint) => {
            let editSkillModel = $scope.oModel.editSkill;
            editSkillModel.user = user;
            editSkillModel.skill = skill;
            editSkillModel.point = skillPoint;
            editSkillModel.skillCategory = skillCategory;

            $scope.oModals.skillEditor = $uibModal.open({
                templateUrl: 'skill-editor.html',
                scope: $scope,
                size: 'md'
            });
        };

        /*
        * Display skill initiator.
        * */
        $scope.displayAddSkill = (skillCategory) => {
            $scope.oModel.addSkill.user = user;
            $scope.oModel.addSkill.point = 0;
            $scope.oModel.addSkill.skillCategory = skillCategory;

            $scope.oModals.skillInitiator = $uibModal.open({
                templateUrl: 'skill-initiator.html',
                scope: $scope,
                size: 'md'
            });
        };

        /*
        * Called when skill is updated.
        * */
        $scope.ngOnEditSkill = (info) => {

            // Block application UI.
            $ui.blockAppUI();

            $personalSkill
                .editPersonalSkill(info.skillCategoryId, info.skillId, info.point)
                .then((entity) => {
                    $scope.$applyAsync(() => {
                        // Close modal dialog.
                        if ($scope.oModals.skillEditor)
                            $scope.oModals.skillEditor.dismiss();

                        $scope.ngOnInit();
                    });
                })
                .catch((error) => {
                    $scope.$applyAsync(() => {
                        $ui.unblockAppUI();
                    });
                });
        };

        /*
        * Called when skill is added.
        * */
        $scope.ngOnAddSkill = (info) => {

            // Block application UI.
            $ui.blockAppUI();

            $personalSkill
                .addPersonalSkill(info.skillCategoryId, info.skillId, info.point)
                .then((entity) => {
                    $scope.$applyAsync(() => {
                        // Close modal dialog.
                        if ($scope.oModals.skillInitiator)
                            $scope.oModals.skillInitiator.dismiss();

                        $scope.ngOnInit();
                    });
                })
                .catch((error) => {
                    console.log(error);
                    $scope.$applyAsync(() => {
                        $ui.unblockAppUI();
                    });
                });
        };

        /*
        * Base on value to find progress bar type.
        * */
        $scope.findProgressBarType = (value) => {
            if (!value || value < 20)
                return 'danger';


            if (20 <= value && value <= 50)
                return 'warning';

            if (50 < value && value < 70)
                return 'success';

            return 'info';
        };

        /*
        * Display add technical skill modal dialog.
        * */
        $scope.displayAddSkillCategory = () => {
            // Update user id.
            $scope.oModel.skillCategory.id = null;
            $scope.oModel.skillCategory.userId = user.id;
            $scope.oModals.addSkillCategory = $uibModal
                .open({
                    templateUrl: 'add-skill-category.html',
                    scope: $scope,
                    size: 'lg'
                })
        };

        /*
        * Called when skill category is loaded.
        * */
        $scope.loadSkillCategory = () => {
            let skillCategory = $scope.oModel.skillCategory;
            if (!skillCategory.id) {
                return {
                    userId: skillCategory.userId
                }
            }

            return $skillCategory
                .getSkillCategories({ids: [skillCategory.id]})
                .then((loadSkillCategoriesResult) => {
                    let skillCategories = loadSkillCategoriesResult.records;
                    return skillCategories[0];
                });
        };


        /*
        * Display edit technical skill modal dialog.
        * */
        $scope.displayEditSkillCategory = (id) => {
            $scope.oModel.skillCategory.id = id;

            // Find edit skill category modal.
            $scope.oModals.editSkillCategory = $uibModal
                .open({
                    templateUrl: 'edit-skill-category.html',
                    scope: $scope,
                    size: 'lg'
                });
        };

        /*
        * Called when skill category is confirmed to be added.
        * */
        $scope.addSkillCategory = (userId, name, encodedPhoto) => {

            // Dismiss the modal dialog.
            let modal = $scope.oModals.addSkillCategory;
            if (modal) {
                modal.dismiss();
                modal = null;
            }

            // Block app UI.
            $ui.blockAppUI();

            $skillCategory
                .addSkillCategory(userId, name, encodedPhoto)
                .then((skillCategory) => {
                    $scope.technicalSkills.push(skillCategory);
                    $ui.unblockAppUI();
                    $scope.$applyAsync();
                })
                .catch(() => {
                    $ui.unblockAppUI();
                    $scope.$applyAsync();
                });
        };

        /*
        * Called when skill category is confirmed to be edited.
        * */
        $scope.editSkillCategory = (id, userId, name, encodedPhoto) => {
            console.log(`id = ${id} | userId = ${userId} | name = ${name} | encodedPhoto = ${encodedPhoto}`);
            // Block app UI.
            $ui.blockAppUI();

            $skillCategory
                .editSkillCategory(id, userId, name, encodedPhoto)
                .then((skillCategory) => {
                    // Find the skill category in list and update it.
                    let skillCategories = $scope.technicalSkills;
                    if (!skillCategories || !skillCategories.length)
                        return skillCategory;

                    for (let index = 0; index < skillCategories.length; index++) {
                        let item = skillCategories[index];
                        if (item.id !== skillCategory.id)
                            continue;

                        skillCategories[index] = skillCategory;
                    }

                    return skillCategory;
                })
                .catch(() => {
                    return null;
                })
                .then(() => {

                    // Dismiss the modal dialog.
                    let modal = $scope.oModals.editSkillCategory;
                    if (modal)
                        modal.dismiss();

                    $ui.unblockAppUI();
                    $scope.$applyAsync();
                });

        };
        /*
        * Called when skill category selection is cancelled.
        * */
        $scope.cancelSkillCategorySelection = () => {

            let modal = $scope.oModals.addSkillCategory;
            if (!modal)
                return;

            modal.dismiss();
            modal = null;
        };

        /*
        * Cancel skill editor.
        * */
        $scope.cancelSkillCategoryEditor = () => {
            let modal = $scope.oModals.editSkillCategory;
            if (!modal)
                return;

            modal.dismiss();
            modal = null;
        };

        //#endregion
    });
};