module.exports = (ngModule) => {
    ngModule.controller('dashboardController', (
        urlStatesConstant, cacheKeyConstant,
        $user,
        $userDescription, $skillCategory, $project, $appCache,
        $translate,
        $uibModal, $state, $ui, $ngConfirm,
        $timeout,
        $scope) => {

        //#region Properties

        // Constants reflection.
        $scope.urlStatesConstant = urlStatesConstant;

        // User information to watch.
        $scope.user = null;

        // List of user descriptions.
        $scope.userDescriptions = [];

        // List of user categories.
        $scope.technicalSkills = [];

        // List of user projects.
        $scope.userProjects = [];

        // List of modals instance.
        $scope.oModals = {
            userPicker: null,
            avatarPicker: null
        };

        //#endregion

        //#region Methods

        /*
        * Called when component is initialized.
        * */
        $scope.ngOnInit = () => {
        };

        /*
        * Display user selector modal.
        * */
        $scope.displayUserModal = () => {
            $scope.oModals.userPicker = $uibModal.open({
                templateUrl: 'user-selector.html',
                scope: $scope,
                size: 'lg'
            });
        };

        /*
        * Called when an user is selected.
        * */
        $scope.selectUser = (user) => {
            // Update user.
            $scope.user = user;

            // Put user to cache.
            $appCache.put(cacheKeyConstant.user, user);

            // Close the user picker.
            if ($scope.oModals.userPicker) {
                $scope.oModals.userPicker.dismiss();
                $scope.oModals.userPicker = null;
            }

            $userDescription
                .getUserDescriptions({userIds: [user.id]})
                .then((result) => {
                    $scope.$apply(() => {
                        $scope.userDescriptions = result.records;
                    });
                });

            // Go to technical skills list.
            const urlStateTechnicalSkill = urlStatesConstant.technicalSkill;
            $state.go(urlStateTechnicalSkill.name);
        };

        /*
        * Called when an image is selected.
        * */
        $scope.ngOnSelectAvatar = (image, base64Image) => {

            // Modal is available. Dismiss it.
            if ($scope.oModals.avatarPicker) {
                $scope.oModals.avatarPicker.dismiss();
                $scope.oModals.avatarPicker = null;
            }

            if (!base64Image)
                return;

            // Update profile photo.
            $user
                .editProfilePhoto($scope.user.id, base64Image)
                .then((user) => {
                    $ngConfirm({
                        title: ' ',
                        content: `<b class="text-success">{{'MSG_AVATAR_CHANGE_SUCCESS' | translate}}</b>`,
                        buttons: {
                            ok: {
                                text: $translate.instant('TITLE_OK'),
                                btnClass: 'btn btn-default'
                            }
                        }
                    });

                    // Update user information.
                    $scope.user = user;
                });
        };

        /*
        * Display avatar picker modal dialog.
        * */
        $scope.displayAvatarPicker = () => {
            $scope.oModals.avatarPicker = $uibModal
                .open({
                    templateUrl: 'avatar-selector.html',
                    scope: $scope,
                    size: 'lg'
                });
        }


        //#endregion
    });
};