const IN = require('typeorm').In;

module.exports = (ngModule) => {
    ngModule.service('$project', (repositoryConstant,
                                  $db) => {


        return {

            /*
            * Get user projects by using specific conditions.
            * */
            loadProjects: (conditions) => {

                return $db
                    .getRepository(repositoryConstant.project)
                    .then((projectRepository) => {

                        // Project search options.
                        let options = {};

                        // Conditions are defined.
                        if (conditions) {
                            if ((conditions.ids instanceof Array) && conditions.ids.length > 0)
                                options['id'] = IN(conditions.ids);

                            if ((conditions.userIds instanceof Array) && conditions.userIds.length > 0)
                                options['userId'] = IN(conditions.userIds);
                        }

                        // Initialize response result.
                        let result = {
                            records: [],
                            total: 0
                        };

                        let pGetRecordsPromise = projectRepository
                            .find({
                                where: options,
                                relations: ['projectSkills', 'projectSkills.skill', 'projectResponsibilities', 'projectResponsibilities.responsibility']
                            })
                            .then((projects) => {
                                result.records = projects;
                            })
                            .catch((error) => {
                                console.log(error);
                                result.records = [];
                            });


                        return Promise
                            .all([pGetRecordsPromise])
                            .then(() => {
                                return result;
                            });
                    })
                    .catch(() => {
                        return null;
                    });
            },

            /*
            * Add user project with specific conditions.
            * */
            addProject: (userId, name, description, startedTime, finishedTime, responsibilityIds, skillIds) => {

                return $db
                    .getRepository(repositoryConstant.project)
                    .then((projectRepository) => {
                        let project = {
                            userId: userId,
                            name: name,
                            description: description,
                            startedTime: startedTime,
                            finishedTime: finishedTime,
                            projectResponsibilities: null,
                            projectSkills: null
                        };

                        if (responsibilityIds && responsibilityIds.length) {
                            project['projectResponsibilities'] = responsibilityIds.map((id) => {
                                return {responsibilityId: id}
                            });
                        }

                        if (skillIds && skillIds.length) {
                            project['projectSkills'] = skillIds.map((id) => {
                                return {skillId: id};
                            });
                        }
                        return projectRepository.save(project);
                    })
                    .catch((error) => {
                        console.log(error);
                        return null;
                    });
            },

            /*
            * Edit project by searching for its id.
            * */
            editProject: (id, name, description, startedTime, finishedTime, responsibilityIds, technologyIds) => {
                return $db
                    .getConnection()
                    .then((connection) => {
                        return connection.transaction(() => {
                            return {
                                projectRepository: connection.getRepository('Project'),
                                projectResponsibilityRepository: connection.getRepository('ProjectResponsibility'),
                                projectSkillRepository: connection.getRepository(repositoryConstant.projectSkill)
                            };
                        });
                    })
                    .then((data) => {

                        let projectRepository = data.projectRepository;
                        return projectRepository
                            .findOne({
                                where: {
                                    id: id
                                }
                            })
                            .then((project) => {
                                if (!project)
                                    throw 'Cannot find project';

                                data['project'] = project;
                                return data;
                            });
                    })
                    .then((data) => {

                        let project = data.project;
                        let projectRepository = data.projectRepository;
                        let projectResponsibilityRepository = data.projectResponsibilityRepository;
                        let projectTechnologyRepository = data.projectTechnologyRepository;

                        project.name = name;
                        project.description = description;
                        project.startedTime = startedTime;
                        project.finishedTime = finishedTime;

                        // Promise which needs to be resolved.
                        let promises = [];

                        //#region Update responsibility

                        if (responsibilityIds && responsibilityIds.length) {
                            let pDeleteProjectResponsibilityPromise = projectResponsibilityRepository
                                .delete({
                                    projectId: project.id
                                })
                                .then(() => {
                                    let projectResponsibilities = responsibilityIds
                                        .map((id) => {
                                            return {
                                                projectId: project.id,
                                                responsibilityId: id
                                            }
                                        });

                                    return projectResponsibilityRepository.save(projectResponsibilities);
                                });

                            promises.push(pDeleteProjectResponsibilityPromise);
                        }

                        //#endregion

                        //#region Technology

                        if (technologyIds && technologyIds.length) {
                            let pDeleteProjectSkillPromise = projectSkillRepository
                                .delete({
                                    projectId: project.id
                                })
                                .then(() => {
                                    let projectTechnologies = skillIds.map((id) => {
                                        return {
                                            projectId: project.id,
                                            skillId: id
                                        };
                                    });

                                    return projectTechnologyRepository.save(projectTechnologies);
                                });

                            promises.push(pDeleteProjectSkillPromise);
                        }

                        //#endregion

                        promises.push(projectRepository.save(project));

                        return Promise.all(promises);
                    });
            }
        };
    });
};