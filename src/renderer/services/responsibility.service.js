const IN = require('typeorm').In;

module.exports = (ngModule) => {
    ngModule.service('$responsibility', (repositoryConstant,
                                         $db) => {


        return {

            /*
            * Get user projects by using specific conditions.
            * */
            loadResponsibilities: (conditions) => {

                return $db
                    .getRepository(repositoryConstant.responsibility)
                    .then((responsibilityRepository) => {

                        // Project search options.
                        let options = {};

                        // Conditions are defined.
                        if (conditions) {
                            if ((conditions.ids instanceof Array) && conditions.ids.length > 0)
                                options['id'] = IN(conditions.ids);
                        }

                        // Initialize response result.
                        let result = {
                            records: [],
                            total: 0
                        };

                        let pGetRecordsPromise = responsibilityRepository
                            .find({
                                where: options,
                                relations: ['projectResponsibilities']
                            })
                            .then((responsibilities) => {
                                result.records = responsibilities;
                            });
                        return pGetRecordsPromise
                            .then(() => {
                                return result;
                            });
                    })
                    .catch((error) => {
                        console.log(error);
                        throw error;
                    });
            },

            /*
            * Edit responsibility using specific information.
            * */
            editResponsibility: (id, name) => {
                return $db
                    .getRepository(repositoryConstant.responsibility)
                    .then((responsibilityRepository) => {
                        return responsibilityRepository
                            .findOne({
                                where: {
                                    id: id
                                }
                            })
                            .then((responsibility) => {
                                if (!responsibility)
                                    throw 'No responsibility is found';

                                // Update responsibility information.
                                responsibility.name = name;
                                responsibility.lastModifiedTime = new Date().getTime();

                                return responsibilityRepository.save(responsibility);
                            });
                    });
            },

            /*
            * Find responsibility and delete it by using its id.
            * */
            deleteResponsibility: (id) => {
                return service.getRepository()
                    .then((responsibilityRepository) => {
                        return responsibilityRepository.delete({
                            id: id
                        });
                    });
            },

            /*
            * Add responsibility to database.
            * */
            addResponsibility: (name) => {
                return $db
                    .getRepository(repositoryConstant.responsibility)
                    .then((responsibilityRepository) => {
                        return responsibilityRepository
                            .findOne({
                                name: name
                            })
                            .then((responsibility) => {
                                if (responsibility)
                                    throw 'Responsibility already exists.';

                                responsibility = {
                                    name: name,
                                    createdTime: new Date().getTime()
                                };

                                return responsibilityRepository
                                    .save(responsibility);
                            });
                    })

            }
        };
    });
};