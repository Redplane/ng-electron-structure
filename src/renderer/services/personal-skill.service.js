module.exports = (ngModule) => {
    ngModule.service('$personalSkill', ($db) => {


        let service = {

            getRepository: () => {
                return $db
                    .getConnection()
                    .then((connection) => {
                        return connection.getRepository('PersonalSkill');
                    });
            },

            /*
            * Add personal skill into database.
            * */
            editPersonalSkill: (skillCategoryId, skillId, point) => {
                return service.getRepository()
                    .then((personalSkillRepository) => {

                        // Find the entity by using unique information.
                        return personalSkillRepository
                            .findOne({
                                where: {
                                    skillCategoryId: skillCategoryId,
                                    skillId: skillId
                                }
                            })
                            .then((entity) => {

                                // Entity is not found.
                                if (!entity)
                                    throw 'No skill category has been found';

                                // Update entity information.
                                entity.point = point;
                                return personalSkillRepository.save(entity);
                            });
                    });
            },

            /*
            * Add personal skill into database.
            * */
            addPersonalSkill: (skillCategoryId, skillId, point) => {
                return service.getRepository()
                    .then((personalSkillRepository) => {
                        return personalSkillRepository
                            .findOne({
                                skillCategoryId: skillCategoryId,
                                skillId: skillId
                            })
                            .then((skillCategory) => {
                                if (skillCategory)
                                    throw 'Skill category already existed.';

                                return null;
                            })
                            .then(() => {
                                return personalSkillRepository
                                    .save({
                                        skillCategoryId: skillCategoryId,
                                        skillId: skillId,
                                        point: point,
                                        createdTime: 0
                                    });
                            });
                    });
            }
        };

        return service;
    });

};