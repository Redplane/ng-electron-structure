const IN = require('typeorm').In;

module.exports = (ngModule) => {
    ngModule.service('$userDescription', ($db) => {


        let service = {

            getRepository: () => {
                return $db.getConnection()
                    .then((connection) => {
                        return connection.getRepository('UserDescription');
                    });
            },

            /*
            * Get users by using specific conditions.
            * */
            getUserDescriptions: (conditions) => {
                return service.getRepository()
                    .then((userRepository) => {

                        let options = {};

                        if (conditions) {
                            if ((conditions.ids instanceof Array) && conditions.ids.length > 0)
                                options['id'] = IN(conditions.ids);

                            if ((conditions.userIds instanceof Array) && conditions.userIds.length > 0)
                                options['userId'] = IN(conditions.userIds);
                        }

                        // Query result.
                        let result = {
                            records: [],
                            total: 0
                        };

                        let pGetRecordsPromise = userRepository
                            .find({
                                where: options
                            })
                            .then((userDescriptions) => {
                                result.records = userDescriptions;
                            });


                        return Promise.all([pGetRecordsPromise])
                            .then(() => {
                                return result;
                            })
                    });
            }
        };

        return service;
    });

};