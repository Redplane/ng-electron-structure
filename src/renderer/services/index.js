module.exports = (ngModule) => {
    require('./ui.service')(ngModule);
    require('./user.service')(ngModule);
    require('./user-description.service')(ngModule);
    require('./skill-category.service')(ngModule);
    require('./project.service')(ngModule);
    require('./skill.service')(ngModule);
    require('./personal-skill.service')(ngModule);
    require('./responsibility.service')(ngModule);
    require('./db.service')(ngModule);
};