const IN = require('typeorm').In;

module.exports = (ngModule) => {
    ngModule.service('$user', (
        repositoryConstant,
        $db) => {


        let service = {

            getRepository: () => {
                return $db
                    .getConnection()
                    .then((connection) => {
                        return connection.getRepository('User');
                    });
            },

            /*
            * Get users by using specific conditions.
            * */
            getUsers: (conditions) => {
                console.log(repositoryConstant.user);
                return $db
                    .getRepository(repositoryConstant.user)
                    .then((userRepository) => {

                        // Search options.
                        let options = {};

                        if (conditions) {
                            if ((conditions.ids instanceof Array) && (conditions.ids.length > 0))
                                options['id'] = IN(conditions.ids);
                        }

                        return userRepository
                            .find({
                                where: options
                            })
                            .then((users) => {
                                return {
                                    records: users,
                                    total: users.length
                                }
                            });
                    })
                    .catch((error) => {
                        console.log(error);
                        return null;
                    });
            },

            /*
            * Edit profile photo by search for user id.
            * */
            editProfilePhoto: (id, photo) => {
                return $db
                    .getRepository(repositoryConstant.user)
                    .then((userRepository) => {
                        return userRepository
                            .findOne({
                                where: {
                                    ids: [id]
                                }
                            })
                            .then((user) => {
                                if (!user)
                                    throw 'User is not found';

                                user.photo = photo;
                                return userRepository.save(user);
                            });
                    })
            }
        };

        return service;
    });

};