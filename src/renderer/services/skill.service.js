module.exports = (ngModule) => {

    ngModule.service('$skill', (repositoryConstant,
                                $db) => {
        return {

            /*
            * Get user projects by using specific conditions.
            * */
            getSkills: (conditions) => {

                return $db
                    .getRepository(repositoryConstant.skill)
                    .then((skillRepository) => {

                        // Project search options.
                        let options = {};

                        // Conditions are defined.
                        if (conditions) {
                            if ((conditions.ids instanceof Array) && conditions.ids.length > 0)
                                options['id'] = IN(conditions.ids);
                        }

                        // Initialize response result.
                        let result = {
                            records: [],
                            total: 0
                        };

                        let pGetRecordsPromise = skillRepository
                            .find({
                                where: options
                            })
                            .then((skills) => {
                                result.records = skills;
                            });


                        return Promise.all([pGetRecordsPromise])
                            .then(() => {
                                return result;
                            });
                    });
            },

            /*
            * Search and delete skill by using specific conditions.
            * */
            deleteSkill: (skillId) => {
                // Get database connection.
                return $db.getConnection()
                    .then((dbConnection) => {
                        // Get related repositories.
                        let skillRepository = dbConnection.getRepository(repositoryConstant.skill);
                        let personalSkillRepository = dbConnection.getRepository(repositoryConstant.personalSkill);

                        // Search and delete personal skill.
                        return dbConnection.transaction((transactionEntityManager) => {
                            return personalSkillRepository
                                .delete({
                                    skillId: skillId
                                })
                                .then(() => {
                                    return skillRepository.delete({
                                        id: skillId
                                    });
                                });
                        });
                    });
            },

            /*
            * Add skill by using specific information.
            * */
            addSkill: (name) => {
                // Get repository.
                return $db
                    .getRepository(repositoryConstant.skill)
                    .then((skillRepository) => {

                        let skill = {
                            name: name,
                            createdTime: new Date().getTime()
                        };

                        return skillRepository.insert(skill);
                    });
            },

            /*
            * Edit skill by using specific condition.
            * */
            editSkill: (id, name) => {
                // Find repository.
                return service.getRepository()
                    .then((skillRepository) => {
                        return skillRepository
                            .findOne({
                                id: id
                            })
                            .then((skill) => {
                                if (!skill)
                                    throw 'No skill has been found';

                                // Update skill information and save it.
                                skill.name = name;
                                return skillRepository.save(skill);
                            });
                    })

            }
        };
        return service;
    });
};