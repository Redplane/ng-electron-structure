const typeorm = require('typeorm');
const EntitySchema = typeorm.EntitySchema;
const path = require('path');
const electron = require('electron');
require('sqlite3');
module.exports = (ngModule) => {
    ngModule.service('$db', (toastr) => {

        //#region Properties

        // Instance of database connection
        let dbConnection = null;

        //#endregion

        let out = {

            //#region Methods

            /*
            * Initialize database connection
            * */
            getConnection: (forceReinitialize) => {
                // if (dbConnection == null || forceReinitialize) {
                //     return typeorm
                //         .createConnection({
                //             type: "mssql",
                //             host: "localhost",
                //             port: 65269,
                //             username: "sa",
                //             password: "Redplane1",
                //             database: "PersonalCv",
                //             synchronize: false,
                //             entities: [
                //                 new EntitySchema(require('../models/entities/user')),
                //                 new EntitySchema(require('../models/entities/user-decription')),
                //                 new EntitySchema(require('../models/entities/skill-category-editor')),
                //                 new EntitySchema(require('../models/entities/skill')),
                //                 new EntitySchema(require('../models/entities/personal-skill')),
                //                 new EntitySchema(require('../models/entities/project')),
                //                 new EntitySchema(require('../models/entities/project-technology')),
                //                 new EntitySchema(require('../models/entities/technology')),
                //                 new EntitySchema(require('../models/entities/project-responsibility')),
                //                 new EntitySchema(require('../models/entities/responsibility'))
                //             ]
                //         })
                //         .then((connection) => {
                //             dbConnection = connection;
                //             return dbConnection;
                //         });
                // }
                //
                // return new Promise(resolve => {
                //     resolve(dbConnection);
                // });

                if (dbConnection == null || forceReinitialize) {
                    // Build a absolute path to database.
                    let appPath = electron.remote.app.getAppPath();
                    if (IS_PRODUCTION)
                        appPath = appPath.replace('app.asar', '');
                    const dbPath = path.join(appPath, 'resources/db/PersonalCv.db');
                    return typeorm
                        .createConnection({
                            type: "sqlite",
                            database: dbPath,
                            synchronize: false,
                            entities: [
                                new EntitySchema(require('../models/entities/user')),
                                new EntitySchema(require('../models/entities/user-decription')),
                                new EntitySchema(require('../models/entities/skill-category')),
                                new EntitySchema(require('../models/entities/skill')),
                                new EntitySchema(require('../models/entities/personal-skill')),
                                new EntitySchema(require('../models/entities/project')),
                                new EntitySchema(require('../models/entities/project-skill')),
                                new EntitySchema(require('../models/entities/project-responsibility')),
                                new EntitySchema(require('../models/entities/responsibility'))
                            ]
                        })
                        .then((connection) => {
                            dbConnection = connection;
                            return dbConnection;
                        })
                        .catch((error) => {
                            toastr.error(error);
                            throw error;
                        });
                }

                return new Promise(resolve => {
                    resolve(dbConnection);
                });
            },

            /*
            * Get repository by using name (table name)
            * */
            getRepository: (name) => {
                return out
                    .getConnection()
                    .then((connection) => {
                        return connection.getRepository(name);
                    });
            }

            //#endregion
        };

        return out;
    });
};