const IN = require('typeorm').In;

module.exports = (ngModule) => {
    ngModule.service('$skillCategory', (repositoryConstant,
                                        $db) => {


        return {
            /*
           * Get users by using specific conditions.
           * */
            getSkillCategories: (conditions) => {
                return $db
                    .getRepository(repositoryConstant.skillCategory)
                    .then((skillCategoryRepository) => {

                        // Initialize find option.
                        let options = {};
                        if ((conditions.ids instanceof Array) && conditions.ids.length > 0)
                            options['id'] = IN(conditions.ids);

                        if ((conditions.userIds instanceof Array) && conditions.userIds.length > 0)
                            options['userId'] = IN(conditions.userIds);

                        // Initialize response result.
                        let result = {
                            records: [],
                            total: 0
                        };


                        let pGetRecordsPromise = skillCategoryRepository
                            .find({
                                where: options,
                                relations: ['personalSkills', 'personalSkills.skill']
                            })
                            .then((userSkills) => {
                                result.records = userSkills;
                            });

                        let pGetCountPromise = skillCategoryRepository
                            .count()
                            .then((count) => {
                                result.total = count;
                            });

                        return Promise
                            .all([pGetRecordsPromise, pGetCountPromise])
                            .then(() => {
                                return result;
                            });
                    });
            },

            /*
            * Edit skill category.
            * */
            editSkillCategory: (id, userId, name, photo) => {
                return $db
                    .getRepository(repositoryConstant.skillCategory)
                    .then((skillCategoryRepository) => {
                        return skillCategoryRepository
                            .findOne({
                                id: id
                            })
                            .then((skillCategory) => {
                                if (!skillCategory)
                                    throw 'No skill category is found';

                                return skillCategory;
                            })
                            .then((skillCategory) => {
                                if (name && name.length)
                                    skillCategory.name = name;

                                if (photo && photo.length)
                                    skillCategory.photo = photo;

                                if (userId)
                                    skillCategory.userId = userId;

                                return skillCategoryRepository.save(skillCategory);
                            });
                    })
            },

            /*
            * Edit skill category information.
            * */
            addSkillCategory: (userId, name, photo) => {
                return $db
                    .getRepository(repositoryConstant.skillCategory)
                    .then((skillCategoryRepository) => {
                        return skillCategoryRepository
                            .save({
                                userId: userId,
                                name: name,
                                photo: photo,
                                createdTime: new Date().getTime()
                            });
                    });
            }
        };
    });

};