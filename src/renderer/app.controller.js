module.exports = (ngModule) => {

    // Import ipc renderer.
    const ipcRenderer = require('electron').ipcRenderer;
    const pSharedProcessEvents = require('../shared/constants/event');

    // Import pusher-client lib.
    const Pusher = require('pusher-client');

    /*
    * Application controller definition.
    * */
    ngModule.controller('appController', ($ui, $user, $db, $translate,
                                          apiUrlConstant,
                                          $scope) => {

        //#region Properties

        /*
        * Check whether window is focused or not.
        * */
        $scope.bIsWindowFocused = false;


        //#endregion

        //#region Methods

        /*
        * Called when app instance is initialized.
        * */
        $scope.ngOnInit = () => {
            let pusher = new Pusher('2c6b309a97307c385002', {
                cluster: 'ap1',
                encrypted: true
            });

            let mailChannel = pusher.subscribe('mail-channel');
            mailChannel
                .bind('mail-receive-event', (data) => {
                    if ($scope.bIsWindowFocused)
                        return;

                    data.title = $translate.instant('TITLE_SYSTEM_MESSAGE');

                    // Pass the notification to main process.
                    ipcRenderer.send(pSharedProcessEvents.EVENT_NOTIFICATION_RECEIVED, data);
                });
        };

        /*
        * Called when window is focused.
        * */
        $scope.ngOnWindowFocused = () => {
            $scope.bIsWindowFocused = true;
        };

        /*
        * Called when window is blurred.
        * */
        $scope.ngOnWindowBlurred = () => {
            $scope.bIsWindowFocused = false;
        };

        //#endregion

        //#region Events

        /*
        * Called when main window's focus is lost.
        * */
        ipcRenderer
            .on(pSharedProcessEvents.EVENT_BLUR, $scope.ngOnWindowBlurred);

        /*
        * Called when main window is focused.
        * */
        ipcRenderer
            .on(pSharedProcessEvents.EVENT_FOCUS, $scope.ngOnWindowFocused);

        //#endregion
    });
};